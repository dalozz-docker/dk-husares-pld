# DK-HUSARES-PLD

Recetario para dockerizar app-pld. Solo se necesita
los wars y tener configurado el context para el acceso
a la bd por tomcat.


## Structure

```sh
├───app-pld             
├───tomcat              
│   ├───server-conf     
│   └───server-lib      
├───Dockerfile                  
├───docker-compose.build.yml              
├───docker-compose.dev.yml      
├───sh_build.sh    
├───sh_down.sh     
├───sh_remove_image.sh   
├───sh_up.sh    
└───sh_run.sh    
```

* **app-pld/** Wars a desplegar.
* **tomcat/** Configuraciones del servidor.
* **Dockerfile** Recetario de la imagen, contiene la configuración personalizada.
* **docker-compose.build.yml** Recetario para iniciar la construccion de la imagen.
* **docker-compose.dev.yml** Recetario para iniciar los servicios(Conjunto de containers).
* **sh_build.sh** script para dar inicio a la construcción de la imagen.
* **sh_down.sh** script para terminar los services.
* **sh_remove_image.sh** script para eliminar la imagen de nuestro repo local.
* **sh_up.sh** script para iniciar los servicios.
* **sh_run.sh** script que inicia desde cero la construcción de la imagen y posterior publicación.

## Requirements
At least 2GB of memory and 3.5GB of disk space is required.

## Installation
```sh
# Init process
sh sh_run.sh