FROM tomcat:7-alpine
LABEL "MAINTAINER"="Dalozz <cristhianp00@gmail.com"
ENV APP_DIR=/usr/local/tomcat/webapps
RUN apk update && apk add curl tree vim

#Config files
COPY tomcat/server-conf/context.xml /usr/local/tomcat/conf/context.xml
COPY tomcat/server-conf/tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml

#Libs jars
COPY tomcat/server-lib/ojdbc6-1.0.jar /usr/local/tomcat/lib/ojdbc6-1.0.jar

#Webapp wars
COPY app-pld/patpub.war /usr/local/tomcat/webapps/patpub.war
COPY app-pld/patpub-back.war /usr/local/tomcat/webapps/patpub-back.war
COPY app-pld/patpubST.war /usr/local/tomcat/webapps/patpubST.war
COPY app-pld/configuracion-zp.war /usr/local/tomcat/webapps/configuracion-zp.war


VOLUME $APP_DIR
WORKDIR $APP_DIR
EXPOSE 8080
CMD ["catalina.sh", "run"]
